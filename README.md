# Exercises-for-Programming

Together with En Yang, we (Yao Gao) finished these tasks. Below is a description of the explicit contribution.

### 1st Project:

In the 1st stage, Yao Gao carried out the first stage of coarse filtering, which involved building file structures, reading files, closing files, and assigning values to hash tables. En Yang carried out the tasks involving selecting the right hash function, calculating the hash value, and writing to the file.

In the 2nd stage, Yao Gao  implement the function of writing to and reading from contiguous storage. En Yang calculated the parameters and created the storage space, which included the number of necessary hash functions, k, and the amount of storage space, m, in bits.

### 2nd Project:

Yao Gao implemented the finding of M-Tree, B+ Tree, and Radix tree.

Yang En implemented the all functions of the initial 256-tree.

### Project3:
Yao Gao carried out the construction and finding of search tree.

En Yang carried out the output of search tree.

