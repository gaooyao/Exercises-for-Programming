#ifndef config_head
#define config_head

#define web_page_dir_path "/news.sohu.com" //网页文件存放根目录

#define link_mask_num 7 //网站前缀掩码

#define dir_sp "/" //文件夹分隔符格式

#define char_len sizeof(char)

#define int_len sizeof(int)

#endif